# Youtube timecode generator
A timecode generator that takes a youtube video, and lets you add timecodes as you play through it. 
Currently linear only.
This is useful for annotating a tutorial or video you are studying.

With the given timecodes, you can click on them to seek directly to that position.

Demo at: https://free-straw.surge.sh

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
